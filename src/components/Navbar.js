import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {logout} from '../actions/auth';

const Navbar = ({isAuthenticated, logout}) => {

    const authLinks = (
        <ul>
            <li><Link to={'/noteboard'}>Notes</Link></li>
            <li><Link to={'/account'}>Account</Link></li>
            <li><button onClick={e => logout()}>Logout</button></li>
        </ul>
    );

    const guestLinks = (
        <ul>
            <li><Link to="/register">Register</Link></li>
            <li><Link to="/login">Login</Link></li>
        </ul>
    );

    return (
        <nav id="navbar">
            <h1>Simple Notetaker</h1>
            {isAuthenticated ? authLinks : guestLinks}
        </nav>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
})
export default connect(mapStateToProps, {logout})(Navbar)
