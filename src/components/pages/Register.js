import React, {useState} from 'react';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import {register} from '../../actions/auth';

const Register = ({isAuthenticated, register}) => {

    const [formData, setFormData] = useState({
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        password2: ""
    })
    const {firstname, lastname, email, password, password2} = formData;

    const onChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onSubmit = e => {
        e.preventDefault();

        if(password === password2){
            register(firstname, lastname, email, password);
        }
    }

    if(isAuthenticated){
        return <Redirect to="/noteboard"/>
    }

    return (
        <section id="register">
            <div className="front-container">
                <form onSubmit={e => onSubmit(e)}>
                    <h2>Register</h2>
                    <div className="form-group">
                        <label htmlFor="firstname">First Name</label>
                        <input type="text" name="firstname" id="firstname" value={firstname} onChange={e => onChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">Last Name</label>
                        <input type="text" name="lastname" id="lastname" value={lastname} onChange={e => onChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" name="email" id="email" value={email} onChange={e => onChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" id="password" value={password} onChange={e => onChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password2">Confirm Password</label>
                        <input type="password" name="password2" id="password2" value={password2} onChange={e => onChange(e)}/>
                    </div>
                    <input type="submit" value="Register" className="btn btn-success"/>
                </form>
            </div>
        </section>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {register})(Register)
