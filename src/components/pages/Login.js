import React, {useState} from 'react';
import {connect} from 'react-redux';
import {login} from '../../actions/auth';
import {Redirect} from 'react-router-dom';
import Spinner from '../Spinner';

const Login = ({isAuthenticated, loading, login, error}) => {

    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });
    const {email, password} = formData;

    const onChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onSubmit = e => {
        e.preventDefault();

        login(email, password);
    }

    if(isAuthenticated){
        return <Redirect to="/noteboard"/>
    }

    return (
        <section id="login">
            <div className="front-container">
                <form onSubmit={e => onSubmit(e)}>
                    <h2>Login</h2>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" name="email" id="email" value={email} onChange={e => onChange(e)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" id="password" value={password} onChange={e => onChange(e)}/>
                    </div>

                    { error && (<p className="error-text">{error}</p>) }

                    {
                        loading ?
                            <Spinner size="small"/> :
                            <input type="submit" value="Login" className="btn btn-success"/>
                    }
                </form>
            </div>
        </section>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading,
    error: state.auth.error
})

export default connect(mapStateToProps, {login})(Login)
