import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {getNotes} from '../../actions/note';
import {searchNotes} from '../../actions/note';
import {clearSearch} from '../../actions/note';
import {logout} from '../../actions/auth';
import Note from '../Note';
import AddModal from '../AddModal';
import EditModal from '../EditModal';
import Spinner from '../Spinner';
import {Redirect} from 'react-router-dom';

const NoteBoard = ({expire_time, logout, notes, loading, refined, getNotes, searchNotes, clearSearch}) => {

    const [showAddModal, setShowAddModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [targetNote, setTargetNote] = useState({});

    const [searchValue, setSearchValue] = useState("");

    useEffect(() => {

        if(Date.now() >= expire_time){
            logout();
            <Redirect to='/login'/>
        }else{
            getNotes();
        }
    }, [getNotes, logout, expire_time]);

    const onCreateButton = (e) => {
        e.preventDefault();
        setShowAddModal(true);
    }

    const onNoteButton = (note) => {
        setTargetNote(note);
        setShowEditModal(true);
    }

    const onInputChange = (e) => {
        setSearchValue(e.target.value);

        if(e.target.value !== ""){
            searchNotes(e.target.value);
        }else{
            clearSearch();
        }
    }

    if(loading){
        return <Spinner size="regular"/>
    }

    return (
        <div id="noteboard">
            <form className="control-form">
                <input type="text" name="" id="" placeholder="Search..." value={searchValue} onChange={e => onInputChange(e)}/>
                <button className="btn btn-success" onClick={(e) => onCreateButton(e)}><i className="fas fa-plus"></i> Create</button>
            </form>
            <div className="notes">
                {   
                    (refined === null) ? 
                        notes.map(note => <Note key={note.noteID} title={note.noteTitle} text={note.noteText} onClick={() => onNoteButton(note)}/>) :
                        refined.map(note => <Note key={note.noteID} title={note.noteTitle} text={note.noteText} onClick={() => onNoteButton(note)}/>)
                }
            </div>
            <AddModal show={showAddModal} onClose={() => setShowAddModal(false)}/>
            <EditModal show={showEditModal} note={targetNote} onClose={() => setShowEditModal(false)} />
        </div>
    )
}

const mapStateToProps = state => ({
    notes: state.note.notes,
    loading: state.note.loading,
    refined: state.note.refined,
    expire_time: state.auth.expire_time
})

export default connect(mapStateToProps, {getNotes, searchNotes, clearSearch, logout})(NoteBoard);
