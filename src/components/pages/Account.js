import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {deleteUser} from '../../actions/auth';
import {logout} from '../../actions/auth';
import {Redirect} from 'react-router-dom';

const Account = ({user, deleteUser, expire_time, logout}) => {

    useEffect(() => {

        if(Date.now() >= expire_time){
            logout();
            <Redirect to='/login'/>
        }
    }, [expire_time, logout])

    const onDeleteButton = () => {
        deleteUser(user.userID);
    }

    return (
        <div id="account">
            <button className="btn btn-danger" onClick={() => onDeleteButton()}>Delete Account</button>
        </div>
    )
}

Account.propTypes = {
    user: PropTypes.object.isRequired,
    deleteUser: PropTypes.func.isRequired,
    expire_time: PropTypes.number.isRequired,
    logout: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    user: state.auth.user,
    expire_time: state.auth.expire_time
})

export default connect(mapStateToProps, {deleteUser, logout})(Account);
