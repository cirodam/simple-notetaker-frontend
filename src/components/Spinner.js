import React from 'react';
import spinner from './spinner.gif';

const Spinner = ({size}) => {
    return (
        <div className="spinner-container">
            <img src={spinner} className={`spinner spinner-${size}`} alt='Loading...'/>
        </div>
    )
}

export default Spinner;