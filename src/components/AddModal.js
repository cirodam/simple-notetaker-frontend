import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addNote} from '../actions/note';
import {logout} from '../actions/auth';
import {Redirect} from 'react-router-dom';

const AddModal = ({addNote, show, onClose, expire_time, logout}) => {

    //Make sure the backend token hasn't expired
    useEffect(() => {
        if(Date.now() >= expire_time){
            logout();
            <Redirect to='/login'/>
        }
    }, [expire_time, logout])

    //Initialize formData
    const [formData, setFormData] = useState({
        title: "",
        text: ""
    });

    const {title, text} = formData;

    //Update state when an input has changed
    const onChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    //Add note when the form is submitted and then close
    const onSubmit = (e) => {
        e.preventDefault();
        addNote(title, text);
        onClose();
    }

    if(!show){
        return null;
    }

    return (
        <div className="modal add-modal">
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="modal-header">
                    <button className="btn" onClick={() => onClose()}><i className="fas fa-times"></i></button>
                    <input type="text" name="title" value={title} onChange={(e) => onChange(e)}/>
                </div>
                <div className="modal-content">
                    <textarea value={text} name="text" onChange={(e) => {onChange(e)}}></textarea>
                    <input type="submit" value="Add Note" className="btn btn-success"/>
                </div>
            </form>
        </div>
    )
}

AddModal.propTypes = {
    addNote: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    show: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    expire_time: PropTypes.number.isRequired
}

const mapStateToProps = state => ({
    expire_time: state.auth.expire_time
})

export default connect(mapStateToProps, {addNote, logout})(AddModal);
