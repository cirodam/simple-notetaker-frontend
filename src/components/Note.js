import React from 'react';

const Note = ({title, text, onClick}) => {
    return (
        <div className="note" onClick={() => onClick()}>
            <h2>{title}</h2>
            <p>{text}</p>
        </div>
    )
}

export default Note
