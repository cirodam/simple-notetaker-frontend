import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {deleteNote} from '../actions/note';
import {updateNote} from '../actions/note';
import {logout} from '../actions/auth';
import {Redirect} from 'react-router-dom';

const EditModal = ({deleteNote, updateNote, note, onClose, show, expire_time, logout}) => {

    //Initialize formData
    const [formData, setFormData] = useState({
        title: "",
        text: ""
    })

    useEffect(() => {

        //Logout if backend token has expired
        if(Date.now() >= expire_time){
            logout();
            <Redirect to='/login'/>

        }else{
            //Otherwise try to set formData to the note passed in
            setFormData({
                title: note.noteTitle || "",
                text: note.noteText || ""
            })
        }
    }, [note, expire_time, logout]);

    const {title, text} = formData;

    const onChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onSubmit = (e) => {
        e.preventDefault();
        updateNote(note.noteID, title, text);
        onClose();
    }

    const onDeleteButton = (e) => {
        e.preventDefault();
        deleteNote(note.noteID);
        onClose();
    }

    if(!show){
        return null;
    }

    return (
        <div className="modal edit-modal">
            <form onSubmit={(e) => onSubmit(e)}>
                <div className="modal-header">
                    <button className="btn btn-exit" onClick={() => onClose()}><i className="fas fa-times"></i></button>
                    <input type="text" name="title" value={title} onChange={(e) => onChange(e)} />
                    <button type="button" className="btn btn-trash" onClick={(e) => onDeleteButton(e)}><i className="fas fa-trash"></i></button>
                </div>
                <div className="modal-content">
                    <textarea value={text} name="text" onChange={(e) => {onChange(e)}}></textarea>
                    <input type="submit" value="Update" className="btn btn-success"/>
                </div>
            </form>
        </div>
    )
}

EditModal.propTypes = {
    deleteNote: PropTypes.func.isRequired,
    updateNote: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    note: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    expire_time: PropTypes.number.isRequired
}

const mapStateToProps = state => ({
    expire_time: state.auth.expire_time
})

export default connect(mapStateToProps, {deleteNote, updateNote, logout})(EditModal)
