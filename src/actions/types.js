export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";
export const DELETE_USER = "DELETE_USER";
export const LOADING_USER = "LOADING_USER"

export const GET_NOTES = "GET_NOTES";
export const NOTE_ERROR = "NOTE_ERROR";
export const ADD_NOTE = "ADD_NOTE";
export const DELETE_NOTE = "DELETE_NOTE";
export const UPDATE_NOTE = "UPDATE_NOTE";
export const SEARCH_NOTES = "SEARCH_NOTES";
export const CLEAR_SEARCH = "CLEAR_SEARCH";
export const LOADING_NOTE = "LOADING_NOTE";