import axios from "axios";
import {API_ROOT} from '../const';
import {REGISTER_SUCCESS, REGISTER_FAIL, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, DELETE_USER, LOADING_USER} from '../actions/types';

export const register = (firstName, lastName, email, password) => async dispatch => {

    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({firstName, lastName, email, password});

    try {
        const res = await axios.post(API_ROOT + "/users", body, options);
        dispatch({type: REGISTER_SUCCESS, payload: res.data});
    } catch (err) {
        dispatch({type: REGISTER_FAIL});
    }
}

export const login = (email, password) => async dispatch => {

    dispatch({type: LOADING_USER});
    const options = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({email, password});

    try {
        const res = await axios.post(API_ROOT + "/auth", body, options);
        dispatch({type: LOGIN_SUCCESS, payload: res.data});
    } catch (err) {
        dispatch({type: LOGIN_FAIL, payload: err.response.data.error});
    }
}

export const logout = () => async dispatch => {
    dispatch({type: LOGOUT});
}

export const deleteUser = (userID) => async dispatch => {

    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.delete(API_ROOT + `/users/${userID}`, options);
        dispatch({type: DELETE_USER, payload: res.data});

    } catch (err) {
        dispatch({type: LOGIN_FAIL});
    }
}