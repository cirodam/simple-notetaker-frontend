import axios from "axios";
import { ADD_NOTE, CLEAR_SEARCH, DELETE_NOTE, GET_NOTES, LOADING_NOTE, NOTE_ERROR, SEARCH_NOTES, UPDATE_NOTE } from "./types";
import { API_ROOT } from "../const";

//Get Notes belonging to this user from the backend
export const getNotes = () => async dispatch => {

    dispatch({type:LOADING_NOTE});
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        const res = await axios.get(API_ROOT + "/notes", options);
        dispatch({type: GET_NOTES, payload: res.data});

    } catch (err) {
        dispatch({type: NOTE_ERROR});
    }
}

//Add a note to this users notes and update state
export const addNote = (noteTitle, noteText) => async dispatch => {

    dispatch({type:LOADING_NOTE});
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };
    const body = JSON.stringify({noteTitle, noteText});

    try {
        const res = await axios.post(API_ROOT + "/notes", body, options);
        dispatch({type: ADD_NOTE, payload: res.data});

    } catch (err) {
        dispatch({type: NOTE_ERROR});
    }
}

//Delete this note and update the state
export const deleteNote = (noteID) => async dispatch => {

    dispatch({type:LOADING_NOTE});
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };

    try {
        await axios.delete(API_ROOT + `/notes/${noteID}`, options);
        dispatch({type: DELETE_NOTE, payload: noteID});

    } catch (err) {
        dispatch({type: NOTE_ERROR});
    }
}

//Update this note and then update the state
export const updateNote = (noteID, noteTitle, noteText) => async dispatch => {

    dispatch({type:LOADING_NOTE});
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-auth-token': localStorage.getItem('token')
        }
    };
    const body = JSON.stringify({noteTitle, noteText});

    try {
        const res = await axios.put(API_ROOT + `/notes/${noteID}`, body, options);
        dispatch({type: UPDATE_NOTE, payload: res.data});

    } catch (err) {
        dispatch({type: NOTE_ERROR});
    }
}

//Update the state based on the provided search term
export const searchNotes = (term) => async dispatch => {

    dispatch({type: SEARCH_NOTES, payload: term});
}

//Clear the previous search term from the state
export const clearSearch = () => async dispatch => {

    dispatch({type: CLEAR_SEARCH});
}

