import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {Provider} from 'react-redux';
import Navbar from './components/Navbar';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import NoteBoard from './components/pages/NoteBoard';
import PrivateRoute from './components/routing/PrivateRoute';
import Account from './components/pages/Account';
import Landing from './components/pages/Landing';
import store from './store';
import './App.css';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <Switch>
            <Route exact path = "/login" component={Login} />
            <Route exact path = "/register" component={Register} />
            <Route exact path = "/" component={Landing} />
            <PrivateRoute exact path = "/noteboard" component={NoteBoard} />
            <PrivateRoute exact path = "/account" component={Account} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  );
}

export default App;
