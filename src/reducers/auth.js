import {DELETE_USER, LOADING_USER, LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT, REGISTER_FAIL, REGISTER_SUCCESS, NOTE_ERROR} from '../actions/types';


const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: (localStorage.getItem('token') ? true : false),
    expire_time: null,
    user: null,
    loading: false,
    error: null
}

export default function reducer(state = initialState, {type, payload}){

    switch (type) {
        case LOADING_USER:
            return {
                ...state,
                loading: true
            }
        case REGISTER_SUCCESS:
        case LOGIN_SUCCESS:
            localStorage.setItem('token', payload.token);
            return {
                ...state,
                isAuthenticated: true,
                token: payload.token,
                expire_time: payload.expires,
                user: payload.user,
                loading: false
            };
    
        case REGISTER_FAIL:
        case LOGIN_FAIL:
            localStorage.removeItem('token');
            return {
                ...state,
                isAuthenticated: false,
                loading: false,
                token: null,
                expire_time: null,
                error: payload
            };

        case NOTE_ERROR:
            localStorage.removeItem('token');
            return {
                ...state,
                isAuthenticated: false,
                loading: false,
                token: null,
                expire_time: null
            };

        case LOGOUT:
        case DELETE_USER:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                user: null,
                loading: false,
                expire_time: null,
                error: null
            }
        default:
            return state;
    }
}