import { ADD_NOTE, CLEAR_SEARCH, DELETE_NOTE, GET_NOTES, LOADING_NOTE, SEARCH_NOTES, UPDATE_NOTE } from "../actions/types";

const initialState = {
    notes: [],
    refined: null,
    loading: false
}

export default function reducer(state = initialState, {type, payload}){
    switch (type) {
        case LOADING_NOTE:
            return {
                ...state,
                loading: true
            }
        case GET_NOTES:
            return {
                ...state, 
                notes: payload,
                loading: false
            }
        case ADD_NOTE:
            return {
                ...state, 
                notes: [payload, ...state.notes],
                loading: false
            }
        case DELETE_NOTE:
            return {
                ...state,
                notes: state.notes.filter(note => note.noteID !== payload),
                loading: false
            }
        case UPDATE_NOTE:
            return {
                ...state,
                notes: state.notes.map(note => note.noteID === payload.noteID ? payload : note),
                loading: false
            }
        case SEARCH_NOTES:
            return {
                ...state,
                refined: state.notes.filter(note => note.noteTitle.includes(payload))
            }
        case CLEAR_SEARCH:
            return {
                ...state,
                refined: null
            }
        default:
            return state;
    }
}